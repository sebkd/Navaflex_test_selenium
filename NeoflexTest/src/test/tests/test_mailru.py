"""Модуль тест-кейс """
import logging
import os
import sys
import time

import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

sys.path.insert(0, os.path.join(os.getcwd(), ''))
from src.test.steps.MailActions import MailActions
from src.test.Model.MailModel import MailModel
from src.test.common.variables import LOGGER_NEOFLEX
from src.test.logs.decos import log
from src.test.common.data import LOGINUSER_OTHER, DATA_MAIL, DATA_TEST

LOGGER = logging.getLogger(LOGGER_NEOFLEX)


class TestLogin:

    def setup(self):
        self.driver = None
        self.service = None
        if 'chrome' in DATA_TEST['drive']:
            self.service = Service(DATA_TEST['drive'])
            self.driver_options = Options()
            self.driver_options.add_argument('start-maximized')

            self.driver = webdriver.Chrome(service=self.service,
                                           options=self.driver_options)

        self.driver.get('https://account.mail.ru/login/?mode=simple')

        self.login_user = MailActions(driver=self.driver)

        self.mail = MailModel()

        LOGGER.info('driver функция setup')

    def teardown(self):
        self.driver.close()
        self.driver.quit()
        LOGGER.info('teardown driver закрыт')

    @pytest.fixture()
    def login(self):
        """Функция входа в почту"""
        self.login_user.login()
        LOGGER.info('Вызов фикстура login')

    @pytest.fixture()
    def create_mail(self, login):
        """Функция создания нового письма"""
        self.login_user.create_mail()
        LOGGER.info('Вызов фикстура create_mail')

    @pytest.fixture()
    def fill_new_mail(self, create_mail):
        """Функция заполнения полей письма"""
        self.mail.set_user_to(DATA_MAIL['to'])
        self.mail.set_subject(DATA_MAIL['subject'])
        self.mail.set_content(DATA_MAIL['content'])
        self.login_user.fill_mail(
            user_to=self.mail.get_user_to(),
            subject=self.mail.get_subject(),
            content=self.mail.get_content()
        )
        LOGGER.info('Вызов фикстура fill_new_mail')

    @pytest.fixture()
    def create_draft(self, fill_new_mail):
        """Функция сохранения черновика"""
        self.login_user.save_draftmail()
        self.login_user.cancel_draftmail()
        time.sleep(1)
        LOGGER.info('Вызов фикстура create_draft')

    @pytest.fixture()
    def create_and_send_mail(self, fill_new_mail):
        self.login_user.send_mail()
        self.login_user.check_template()

    @pytest.mark.scenario2
    @pytest.mark.scenario1
    def test_login(self, login):
        """Тест авторизации пользователя"""
        wait = WebDriverWait(self.driver, 10)
        try:
            wait.until(
                EC.visibility_of_element_located((By.XPATH, LOGINUSER_OTHER['writeMail']))
            )
        except Exception as err:
            LOGGER.info(f'Почта не загружена, ошибка {err}')
        else:
            LOGGER.info('Почта загружена')

        assert 'Входящие' in self.driver.title

    @pytest.mark.scenario2
    @pytest.mark.scenario1
    def test_create_mail(self, login):
        """Тест возможности создания письма"""
        self.login_user.create_mail()
        iframe = self.driver.find_element(By.CLASS_NAME, 'compose-app__compose')

        assert 'Кому' and 'Тема' and 'Копия' in iframe.text

    @pytest.mark.scenario1
    def test_save_draft(self, fill_new_mail):
        """Тест создания черновика"""
        self.login_user.save_draftmail()

        assert self.login_user.cross_to_drafts_from_draft() is True

    @pytest.mark.scenario1
    def test_check_fields_draft(self, create_draft):
        """Тест проверки полей в черновике"""
        self.driver.get('https://e.mail.ru/drafts/')
        wait = WebDriverWait(self.driver, 10)
        try:
            wait.until(
                EC.visibility_of_element_located((By.XPATH, LOGINUSER_OTHER['aOpenDraft']))
            )
        except Exception as err:
            LOGGER.info(f'Почта не загружена, ошибка {err}')
        else:
            LOGGER.info('Почта загружена')
        self.login_user.open_first_draft()
        check_to, check_subject, check_content = self.login_user.check_draft()

        if '\n' in check_content:
            check_content = check_content.split('\n')[0]

        assert self.mail.get_user_to() == check_to and \
               self.mail.get_subject() == check_subject and \
               self.mail.get_content() == check_content

    @pytest.mark.scenario2
    def test_send_mail(self, fill_new_mail):
        """Тест отправки письма"""
        self.login_user.send_mail()

        assert 'Закрыть' in self.login_user.check_template()

    @pytest.mark.scenario2
    def test_check_fields_sent_mail(self, create_and_send_mail):
        """Тест проверки полей в отправленном письме"""
        self.driver.get('https://e.mail.ru/sent/')
        wait = WebDriverWait(self.driver, 10)
        try:
            wait.until(
                EC.visibility_of_element_located((By.XPATH, LOGINUSER_OTHER['aOpenDraft']))
            )
        except Exception as err:
            LOGGER.info(f'Почта не загружена, ошибка {err}')
        else:
            LOGGER.info('Почта загружена')

        self.login_user.open_first_sent_mail()
        check_to, check_subject, check_content = self.login_user.check_sent_mail()

        if '\n' in check_content:
            check_content = check_content.split('\n')[0]

        assert self.mail.get_user_to() == check_to and \
               self.mail.get_subject() == check_subject and \
               self.mail.get_content() == check_content

    @pytest.mark.scenario2
    @pytest.mark.scenario1
    def test_logout(self, login):
        """Тест проверки logout"""
        wait = WebDriverWait(self.driver, 10)
        try:
            wait.until(
                EC.visibility_of_element_located((By.XPATH, LOGINUSER_OTHER['writeMail']))
            )
        except Exception as err:
            LOGGER.info(f'Почта не загружена, ошибка {err}')
        else:
            LOGGER.info('Почта загружена')

        self.login_user.logout()
        time.sleep(1)

        assert 'logout' in self.driver.current_url
