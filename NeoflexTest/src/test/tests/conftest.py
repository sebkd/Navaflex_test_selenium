# content of conftest.py

import pytest


def pytest_configure(config):
    # регистрируется метка `@pytest.mark.scenario1`
    config.addinivalue_line(
        "markers", "scenario1: Сценарий по Действия 1")
    # регистрируется метка `@pytest.mark.scenario2`
    config.addinivalue_line(
        "markers", "scenario2: Сценарий по Действия 2")
