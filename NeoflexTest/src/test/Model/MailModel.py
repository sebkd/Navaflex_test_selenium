

class MailModel:
    def __init__(self):
        self._user_to = None
        self._subject = None
        self._content = None

    def get_user_to(self):
        return self._user_to

    def get_subject(self):
        return self._subject

    def get_content(self):
        return self._content

    def set_user_to(self, user_to):
        self._user_to = user_to

    def set_subject(self, subject):
        self._subject = subject

    def set_content(self, content):
        self._content = content
