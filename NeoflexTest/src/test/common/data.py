"""Данные словарей для steps"""

"""Для MailActions"""
LOGINUSER_LOCATORS = {
        "btnSignIn": ('XPATH', '//button[contains(text(), "Войти")]'),
        "edtUserName": ('XPATH', '//input[contains(@name, "username")]'),
        "edtPassword": ('XPATH', '//input[contains(@name, "password")]'),
        "clckElementWrite": ('XPATH', '//a[@href="/compose/"]'),
        "clckElementCancel": ('XPATH', '//button[data-test-id="cancel"]'),
        "edtTo": ('XPATH', '//div[contains(@class, "inputContainer")]//input'),
        "edtTitle": ('XPATH', '//div[contains(@class, "subject__container")]//input'),
        "edtContent": ('XPATH', '//div[contains(@role, "textbox")]'),
        "btnSaveDraft": ('XPATH', '//button[contains(@data-test-id, "save")]'),
        "btnCancelDraft": ('XPATH', '//button[contains(@data-test-id, "cancel")]'),
        "btnSendMail": ('XPATH', '//button[contains(@data-test-id, "send")]'),
        "aOpenDraft": ('XPATH', '//div[@class="draggable"]//a[contains(@class, "first")]'),
        "aSaveDraft": ('XPATH', '//a[@class="notify__action"]'),
        "edtToCheck": ('XPATH', '//div[contains(@data-type, "to")]'),
        "edtSubjectCheck": ('XPATH', '//input[contains(@name, "Subject")]'),
        "edtContentCheck": ('XPATH', '//div[contains(@role, "textbox")]'),
        "btnProfile": ('XPATH', '//span[contains(@class, "user-name")]'),
        "btnLogout": ('XPATH', '//div[contains(@class, "ph-accounts")]//div[contains(@tabindex, "0")]'),
        "edtCloseSentMail": ('XPATH', '//span[contains(@title, "Закрыть")]'),
        "edtRecipientSent": ('XPATH', '//div[contains(@class,"recipients")]//span[contains(@class, "letter-contact")]'),
        "edtSubjectCheckSent": ('XPATH', '//h2[@class="thread-subject"]'),
        "edtContentCheckSent": ('XPATH', '//div[contains(@class, "letter-body")]'),
    }

"""Рабочие данные"""
LOGINUSER_OTHER = {
    "inputFrame": 'ag_js-popup-frame',
    "link": '//iframe[contains(@class, "ag-popup__frame__layout__iframe")]',
    "src": 'src',
    "writeMail": '//a[@href="/compose/"]',
    "clckElementCancel": '//button[data-test-id="cancel"]',
    "iframe": '//div[class="compose-app__compose"]',
    "aOpenDraft": '//div[@class="draggable"]//a[contains(@class, "first")]',
    "titleDraft": '//div[@]'
}

"""Данные для формирования писем"""
DATA_MAIL = {
    "to": "sebkd@mail.ru",
    "subject": "Very important message",
    "content": "Нет времени на разговоры, "
               "поэтому высылаю рисунок"
}

"""Данные в модуле тест"""
DRIVE_CHROME_PATH = './src/test/drivers/chromedriver'

DATA_TEST = {
    'drive': DRIVE_CHROME_PATH,
}

