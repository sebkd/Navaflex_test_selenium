"""Декораторы"""
import logging
import os
import sys
import traceback

sys.path.insert(0, os.path.join(os.getcwd(), '../../../'))
from src.test.common.variables import LOGGER_NEOFLEX


def log(func_to_log):
    """Функция-декоратор"""

    def log_saver(*args, **kwargs):
        LOGGER = logging.getLogger(LOGGER_NEOFLEX)

        ret = func_to_log(*args, **kwargs)
        LOGGER.info(f'Была вызвана функция {func_to_log.__name__} c параметрами {args}, {kwargs}.'
                     f'Вызов из модуля {func_to_log.__module__}.'
                     f'Вызов из функции {traceback.format_stack()[0].strip().split()[-1]}.')

        return ret

    return log_saver
