import logging
import os
import sys
import time

from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from seleniumpagefactory import PageFactory
from selenium.webdriver.support import expected_conditions as EC

sys.path.insert(0, os.path.join(os.getcwd(), '../../../'))

from src.test.logs.decos import log
import src.test.logs.log_config
from src.test.common.variables import LOGGER_NEOFLEX
from src.test.common.data import LOGINUSER_LOCATORS, LOGINUSER_OTHER, DATA_MAIL
from src.test.Model.MailModel import MailModel

from src.test.common.secret_variables import EMAIL, EMAIL_PASSWORD

LOG = logging.getLogger(LOGGER_NEOFLEX)
LOG.setLevel(logging.INFO)


class MailActions(PageFactory):

    def __init__(self, driver):
        super().__init__()
        self.driver = driver
        self.timeout = 15
        self.highlight = True
        self.driver.implicitly_wait(10)

    locators = LOGINUSER_LOCATORS

    @log
    def login(self):
        """Функция входа в почтовый ящик mail.ru"""

        self.edtUserName.set_text(EMAIL)
        self.edtUserName.send_keys(Keys.ENTER)
        LOG.info('Логин введен и нажат ENTER')

        self.edtPassword.set_text(EMAIL_PASSWORD)
        self.edtPassword.send_keys(Keys.ENTER)
        LOG.info('Пароль введен и нажат ENTER')

    def get_driver(self):
        """Возвращает текущий driver"""
        return self.driver

    @log
    def create_mail(self):
        """Функция создания письма"""
        self.clckElementWrite.click_button()
        LOG.info('Создание письма')

    @log
    def fill_mail(self, user_to, subject, content):
        """Функция заполнения письма"""
        self.edtTo.set_text(user_to)
        self.edtTitle.set_text(subject)
        self.edtContent.set_text(content)
        LOG.info('Заполнены поля to, subject, content')

    @log
    def save_draftmail(self):
        """Функция сохранения черновика"""
        self.btnSaveDraft.click_button()
        LOG.info('Черновик письма сохранен')

    @log
    def cross_to_drafts_from_draft(self):
        """Проверка всплывающего окна после сохранения черновика"""
        try:
            self.aSaveDraft.click_button()
        except Exception as err:
            LOG.info(f'Черновик не сохранен, ошибка {err}')
            return False
        else:
            LOG.info('Переход в черновики')
            return True

    @log
    def cancel_draftmail(self):
        """Функция закрытия черновика"""
        self.btnCancelDraft.click_button()
        LOG.info('Черновик письма закрыт')

    @log
    def send_mail(self):
        """Функция отправки письма"""
        self.btnSendMail.click_button()
        LOG.info('Отправка письма')

    @log
    def check_template(self):
        """Функция проверки отправки письма, через кнопку закрыть всплывающее окно"""
        check = self.edtCloseSentMail.getAttribute('title')
        self.edtCloseSentMail.click_button()
        LOG.info('Проверка отправки письма')
        return check

    @log
    def open_first_draft(self):
        """Функция открытия черновика первого в списке"""
        self.aOpenDraft.click_button()
        LOG.info('Черновик открыт')

    @log
    def open_first_sent_mail(self):
        """Функция открытия первого в списке отправленного письма"""
        self.aOpenDraft.click_button()
        LOG.info('Отправленное письмо открыто')

    @log
    def check_draft(self):
        """Функция проверки полей в черновике"""
        check_to = self.edtToCheck.get_text()
        check_subject = self.edtSubjectCheck.getAttribute('value')
        check_content = self.edtContentCheck.get_text()
        LOG.info('Отправлены поля черновика')
        return check_to, check_subject, check_content

    @log
    def check_sent_mail(self):
        """Функция проверки полей в отправленном письме"""
        check_to = self.edtRecipientSent.getAttribute('title')
        check_subject = self.edtSubjectCheckSent.get_text()
        check_content = self.edtContentCheckSent.get_text()
        LOG.info('Отправлены поля отправленного письма')
        return check_to, check_subject, check_content

    @log
    def logout(self):
        """Функция выхода с почтового ящика"""
        self.btnProfile.click_button()
        self.btnLogout.click_button()
        LOG.info('Logout ')


if __name__ == '__main__':
    pass
# service = Service('./../drivers/chromedriver')
#
# chrome_options = Options()
# chrome_options.add_argument('start-maximized')
#
# driver = webdriver.Chrome(service=service, options=chrome_options)
# driver.get('https://account.mail.ru/login/?mode=simple')
#
# login_user = MailActions(driver=driver)
#
# login_user.login()
# wait = WebDriverWait(driver, 10)
# try:
#     wait.until(
#         EC.visibility_of_element_located((By.XPATH, LOGINUSER_OTHER['writeMail']))
#     )
# except Exception as err:
#     LOG.info(f'Почта не загружена, ошибка {err}')
# else:
#     LOG.info('Почта загружена')
#
# if 'Входящие' in driver.title:
#     LOG.info('Тест login пройден')

# login_user.create_mail()
#
# login_user.fill_mail()
# login_user.save_draftmail()
# if not login_user.cross_to_drafts_from_draft():
#     LOG.info('Тест save_draft не пройден')
#     login_user.cancel_draftmail()
#     time.sleep(2)
# driver.get('https://e.mail.ru/drafts/')
# wait = WebDriverWait(driver, 10)
# try:
#     wait.until(
#         EC.visibility_of_element_located((By.XPATH, LOGINUSER_OTHER['aOpenDraft']))
#     )
# except Exception as err:
#     LOG.info(f'Почта не загружена, ошибка {err}')
# else:
#     LOG.info('Почта загружена')
# login_user.open_first_draft()
# login_user.check_draft()

# login_user.logout()
# mail = MailModel()
# mail.set_user_to(DATA_MAIL['to'])
# mail.set_subject(DATA_MAIL['subject'])
# mail.set_content(DATA_MAIL['content'])
#
# login_user.create_mail()
# login_user.fill_mail(
#     user_to=mail.get_user_to(),
#     subject=mail.get_subject(),
#     content=mail.get_content()
# )
#
# login_user.send_mail()
# login_user.check_template()
# driver.get('https://e.mail.ru/sent/')
#
# wait = WebDriverWait(driver, 10)
# try:
#     wait.until(
#         EC.visibility_of_element_located((By.XPATH, LOGINUSER_OTHER['aOpenDraft']))
#     )
# except Exception as err:
#     LOG.info(f'Почта не загружена, ошибка {err}')
# else:
#     LOG.info('Почта загружена')
#
# login_user.open_first_sent_mail()
# login_user.check_sent_mail()
#
# print()
# driver.close()
# driver.quit()
